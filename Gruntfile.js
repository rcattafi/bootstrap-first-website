module.exports = function(grunt) {
  require('time-grunt')(grunt);
  require('jit-grunt')(grunt,{
	   useminPrepare: 'grunt-usemin'
  });
  grunt.initConfig({
    sass: {

      dist: {
        files: [{
			expand: true,
			cwd: 'css',
			src: ['*.scss'],
			dest: 'css',
			ext: '.css'
		}]
      }
    },

    watch: {
      files: ['css/*.scss'],
      tasks: ['css']
    },
	browserSync: {
	dev: {
		bsFiles: {
			src : [
				'css/*.css',
				'*.html'
				'js/*.js'
			]
		},
		options: {
			watchTask: true,
			server:{
				baseDir: './'
				}
			}
		}
	},
	
	imagemin: {
         dynamic: {
            files: [{
                expand: true,
                cwd: './',
				scr: 'images/*.{png,gif,jpg,jpeg}',
                dest: 'dist/'
            }]
        },
	},
	
	copy: {
	 html: {
		files: [{
		  expand: true,
		  dot: true,
          cwd:'./',	  
		  src: ['*.html'], 
		  dest: 'dist/', 

		}],
	  },
	},
	cssmin: {
		dist:{}
	},
    uglify: {
		dist:{}
	},
	filerev: {
		options:{
			encoding:'utf8',
			algorithm:'md5',
			length:20,
		},
		files: [{
			src: [
				'dist/js/*.js',
				'dist/css/*.css',
			]
		]}
	},
	
	concat:{
		options: {
			separator: ';'
		},
		dist:{}
		
	},
	useminPrepare:{
		foo:{
			dest: 'utf8'
			src: ['index.html','contacto.html','about.html','precios.html' ]
		},
		options: {
			flow:{
				steps: {
					css: ['cssmin'],
					js:  ['uglify'],
				},
				post:{
					css: [{
						name: 'cssmin'
						createconfig: function(context, block){
							var generated = context.options.generated;
							generate.options = {
								keepSpecialComments: 0,
								rebase: false
							}
						}
					}]
					
				}				
			}
		}
	},
	usemin:{
		html: ['dist/index.html','dist/contacto.html','dist/about.html','dist/precios.html'],
		options: {
			assetsDir:['dist','dist/css','dist/js']
		}
	}
	
  });

 grunt.loadNpmTasks('grunt-contrib-sass');
 grunt.registerTask('css', ['sass']);
 
 grunt.loadNpmTasks('grunt-contrib-watch');
 grunt.loadNpmTasks('grunt-contrib-sync');
 grunt.registerTask('default', ['browserSync', 'watch']);
 
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  
  grunt.registerTask('build', [
  
		'clean',
		'copy',
		'imagemin',
		'useminPrepare',
		'concat',
		'cssmin',
		'uglify',
		'filerev',
		'usemin'
		
		]);
  
   grunt.registerTask('img:compress', ['imagemin']);

};
	
		
